<?php
	if(isset($_POST['lp_get_file'])) {

		ob_clean();
		$lp_opt = get_option('lp_options');

		$filename = "LANParty_" . date('Ymd H:m') . ".xls";


		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/vnd.ms-excel;");

		$fields = $_POST['fields'];
		$select_fields = implode(',',$fields);

		$file_header = preg_replace('/`/','', $select_fields);
		$file_header = explode(',', $file_header);

		echo '<table border="1"><tr>';
			foreach ($file_header as $cell) {
				echo '<td>'.$cell.'</td>';
			}
		echo '</tr>';

		global $wpdb;

		$data = $wpdb->get_results("SELECT {$select_fields} FROM {$lp_opt['players_table']}",ARRAY_A);

		foreach($data as $row) {
		echo '<tr>';
		    array_walk($row, 'cleanData');
		    $r = array_values($row);
		    foreach($r as $val) {
		    	echo '<td>'.$val.'</td>';
		    }
		echo '</tr>';
		}

		echo '</table>';
		exit();
	}
	?>

<?php
	lp_header();
	$lp_opt = get_option('lp_options');

	$checkboxs = lp_export_html();

?>

<section class="wrapper">

	<h2>Export date</h2>
	<form method="POST">
		<table>
			<tr class="head">
				<td>Nume</td>
				<td>Campuri</td>
			</tr>
			<?php echo $checkboxs; ?>
			<tr><td colspan="2"><input type="submit" name="lp_get_file" class="button" value="Descarca"></td></tr>
		</table>

	</form>
</section>