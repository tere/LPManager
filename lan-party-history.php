<?php
	lp_header();
	$lp_opt = get_option('lp_options');

?>

<section class="wrapper">
	<div id="istoric">
		<table>
			<tr class="head">
				<td>Nr. CRT</td>
				<td>Nume</td>
				<td>TabelJucatori</td>
				<td>Jocuri Editie</td>
				<td>Jucatori Inscrisi pe Site</td>
				<td>Data Editie</td>
				<td>Afis Editie</td>
			</tr>
		<?php
			global $wpdb;
			$get_history = "SELECT * FROM {$lp_opt['history_table']}";
			$results = $wpdb->get_results($get_history, ARRAY_A);

			foreach($results as $result) {
				echo '<tr>';
					echo '<td>'.$result['id'].'</td>';
					echo '<td>'.$result['nume'].'</td>';
					echo '<td>'.$result['tabel_jucatori'].'</td>';
					echo '<td>'.$result['jocuri_editie'].'</td>';
					echo '<td>'.$result['jucatori_inscrisi_site'].'</td>';
					echo '<td>'.$result['data_editie'].'</td>';
					echo '<td>'.$result['afis_editie'].'</td>';
				echo '</tr>';
			}
		?>
		</table>
	</div>
</section>