<?php
	lp_header();
	$lp_opt = get_option('lp_options');

	if(isset($_POST['widget_content_submit'])) {
		$shortcode_content = $_POST['shortcode_content'];

		$lp_opt['partners_widget'] = $shortcode_content;
		update_option('lp_options',$lp_opt);
	}
?>

<style>
	table.add_row {
		display:none;
	}

	#show_options {
		background: #fff;
		margin: 10px 0;
		text-align: center;
		position: fixed;
		max-width: 220px;
		top: 60px;
		right: 9px;
	}
</style>


<script>
	jQuery(document).ready(function(){
		var add_row_one = '<table class="add_row_full row"><tr class="head"><td>Imagine</td><td>src</td><td>titlu</td><td>href</td></tr><tr class="image"  data-lp-img-class="full"><td>Link imagine</td><td><input name="image_src" type="text"></td><td><input name="image_title" type="text"></td><td><input data-lp-img-class="full" name="image_href" type="text"></td></tr><tr><td colspan="4"><a href="#" id="adauga_la_widget">Adauga</a></td></tr></table>';
		var add_row_two = '<table class="add_row_two row"><tr class="head"><td>2 Imagini</td><td>src</td><td>titlu</td><td>href</td></tr><tr data-lp-img-class="half" class="image"><td>Prima imagine</td><td><input name="image_src" type="text"></td><td><input name="image_title" type="text"></td><td><input name="image_href" type="text"></td></tr><tr data-lp-img-class="half" class="image"><td>A doua imagine</td><td><input name="image_src" type="text"></td><td><input name="image_title" type="text"></td><td><input name="image_href" type="text"></td></tr><tr><td colspan="4"><a href="#" id="adauga_la_widget">Adauga</a></td></tr></table>';
		var add_row_three = '<table class="add_row_three row"><tr class="head"><td>3 Imagini</td><td>src</td><td>titlu</td><td>href</td></tr><tr class="image" data-lp-img-class="one"><td>Prima imagine</td><td><input name="image_src" type="text"></td><td><input name="image_title" type="text"></td><td><input name="image_href" type="text"></td></tr><tr class="image" data-lp-img-class="one"><td>A doua imagine</td><td><input name="image_src" type="text"></td><td><input name="image_title" type="text"></td><td><input name="image_href" type="text"></td></tr><tr class="image" data-lp-img-class="one"><td>A treia imagine</td><td><input name="image_src" type="text"></td><td><input name="image_title" type="text"></td><td><input name="image_href" type="text"></td></tr><tr><td colspan="4"><a href="#" id="adauga_la_widget">Adauga</a></td></tr></table>';


		jQuery('.wrapper').on('click','.add_row', function(e) {
			var length = jQuery(this).data('lp-row');
			var rowvar = 'add_row_'+length;
			jQuery('.wrapper').children('form').append(eval(rowvar));

		});
		jQuery('.wrapper').on('click', '#adauga_la_widget', function(e) {
			e.preventDefault();
			var temp_content = jQuery('#widget_content').val();
				temp_content += '<div class="row">';
			jQuery(this).parents('table.row').find('.image').each(function(){
				var l_class = jQuery(this).data('lp-img-class'); //class for lenght
				var src = jQuery(this).find('input[name="image_src"]').val();
				var title = jQuery(this).find('input[name="image_title"]').val();
				var href = jQuery(this).find('input[name="image_href"]').val();

				    temp_content += '<a  class="'+l_class+'"  href="'+href+'"><img src="'+src+'" alt="'+title+'" /></a>';


			});
			temp_content += '</div>';
			jQuery('#widget_content').val(temp_content);

			jQuery(this).parents('table.row').remove();

		});
	});
</script>

<section class="wrapper">
	<h2>Widget Sponsori</h2>
	<form action="" method="POST">
		<table id="show_options">
			<tr class="head"><td colspan="3">Optiuni</td></tr>
			<tr>
				<td><a href="javascript:void(0)" class="add_row button" data-lp-row="one">Adauga rand cu o imagine</a></td>
			</tr>
			<tr>
				<td><a href="javascript:void(0)" class="add_row button" data-lp-row="two">Adauga rand cu doua imagini</a></td>
			</tr>
			<tr>
				<td><a href="javascript:void(0)" class="add_row button" data-lp-row="three">Adauga rand cu trei imagini</a></td>
			</tr>
			<tr>
				<td><input type="submit" class="button save_widget button-primary" value="Salveaza" name="widget_content_submit"></td>
			</tr>
		</table>


	    <textarea name="shortcode_content" id="widget_content" style="display:block;width:100%;height:500px" value=""><?php echo stripslashes($lp_opt['partners_widget']); ?></textarea>
		</form>
</section>