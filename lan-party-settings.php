<?php
    $lp_opt =   get_option('lp_options');

	if(isset($_GET['delete'])) {




        $nume = $lp_opt['current_ver_title'];
        $tabel_jucatori = $lp_opt['players_table'];
        $jocuri_editie = '';
        $games = $lp_opt['games'];
        foreach($games as $game) {
        	$g = $game['name'];
        	$temp = signs_up($game['is_team'],$game['name']);
        	$g .= '-'.$temp.',';
        	$jocuri_editie.=$g;
        }
        global $wpdb;
        $jucatori_inscrisi_site = $wpdb->get_var("SELECT COUNT(*) FROM `{$lp_opt['players_table']}` WHERE `cu_echipa`=0");
        $echipe_inscrise_site = $wpdb->get_var("SELECT COUNT(DISTINCT *) FROM `{$lp_opt['players_table']}` WHERE `cu_echipa`=1");
        $data_editie = $lp_opt['current_ver_date'];
        $afis_editie = $lp_opt['current_ver_cover'];


        $data = array(
        	'nume' => $nume,
        	'tabel_jucatori' => $tabel_jucatori,
        	'jocuri_editie' => $jocuri_editie,
        	'jucatori_inscrisi_site' => $jucatori_inscrisi_site,
        	'echipe_inscrise_site' => $echipe_inscrise_site,
        	'data_editie' => $data_editie,
        	'afis_editie' => $afis_editie
        	);

        $params = array(
        	'%s',
        	'%s',
        	'%s',
        	'%d',
        	'%d',
        	'%s',
        	'%s'
        	);
        $wpdb->insert($lp_opt['history_table'],$data,$params);


		$def_lp_options = array(
        'current_ver' => null,
        'current_ver_title' => null,
        'current_ver_date' => null,
        'current_ver_cover' => null,
        'lp_is_open' => 0,
        'players_table' => null,
        'history_table' => 'lp_lan_party_editions',
        'games' => null //0 nu se pot face inscrieri, 1 se pot face
        );


		update_option('lp_options',$def_lp_options);
	}

	if(isset($_GET['startjoc'])) {
		$games = $lp_opt['games'];

		$l = count($games);

		for($i=0;$i<$l;$i++) {
			$games[$i]['status'] = 1;
		}

		$lp_opt['games'] = $games;
		update_option('lp_options',$lp_opt);
	}

	if(isset($_GET['stopjoc'])) {
		$games = $lp_opt['games'];

		$l = count($games);

		for($i=0;$i<$l;$i++) {
			$games[$i]['status'] = 0;
		}

		$lp_opt['games'] = $games;
		update_option('lp_options',$lp_opt);
	}


	lp_add_new_edition();
	lp_update_edition();



	lp_header();

	$lp_opt =   get_option('lp_options');
?>

<script>
	jQuery(document).ready(function() {
		jQuery('#adauga_joc').on('click', function(e) {
			e.preventDefault();
			jQuery('#jocuri').append('<tr><td><input type="text" name="jocuri[]"></td><td><input type="text" name="g_title[]"></td><td><select name="type_jocuri[]" class="game_type"><option value="0">Single</option><option value="1">Team</option></select></td><td><input type="text" name="max_players[]"></td><td><input type="text" name="rules_n_terms[]"></td><td><a href="#" id="sterge_joc" class="button">Sterge joc</a></td></tr>');

		});
		jQuery('table').on('click','#sterge_joc', function(e) {
			e.preventDefault();
			jQuery('#sterge_joc').closest('tr').empty();
		});
	});
</script>

<div class="wrapper">
	<?php if($lp_opt['lp_is_open']) {

		echo '<form action="" method="POST"><table>';
		echo '<tr class="head"><td colspan="3">Setari Editie Curenta</td></tr>';
		echo '<tr><td><a href="'. add_query_arg('delete',$lp_opt['current_ver']) .'" class="a button">Inchide editia curenta</a></td><td><a href="'.add_query_arg('startjoc','1').'" class="a button">Deschide inscrieri</a></td><td><a href="'.add_query_arg('stopjoc','1').'" class="a button">Inchide Inscrieri</a></td></tr>';
		echo '</table><br/><table>';
		echo '<tr class="head"><td>Nume Joc</td><td>Titlu</td><td>Mod : echipa / single</td><td>Nr. Maxim Jucatori / Echipe</td><td>Status inscrieri</td><td>Regulament Joc</td></tr>';
		$games = $lp_opt['games'];
		foreach($games as $game) {
			echo '<tr>';
				echo '<td><input type="text" name="jocuri[]" value="'.$game['name'].'"></td>';
				echo '<td><input type="text" name="g_title[]" value="'.$game['title'].'"></td>';
				echo '<td><input type="text" name="type_jocuri[]" class="game_type" value="'.$game['is_team'].'"></td>';
				echo '<td><input type="text" name="max_players[]" value="'.$game['max_players'].'"></td>';
				echo '<td><input type="text" name="status[]" value="'.$game['status'].'"></td>';
				echo '<td><input type="text" name="rules_n_terms[]" value="'.$game['rules_n_terms'].'"></td>';
			echo '</tr>';

		}
		echo '</table>';



		echo '<p><input type="submit" value="Salveaza setari" name="update_lp_settings" class="button"></p>';




		} else {
			?>
			<form action='' method='POST'>
				<table>
					<tr>
						<td>Nume editie:</td>
						<td><input type="text" name="nume_editie"></td>
					</tr>

					<tr>
						<td>Titlu editie:</td>
						<td><input type="text" name="titlu_editie"></td>
					</tr>
					<tr>
						<td>Data editiei:</td>
						<td><input type="text" name="data_editie"></td>
					</tr>
					<tr>
						<td>Cover editie: (link)</td>
						<td><input type="text" name="cover_editie"></td>
					</tr>
					<tr>
						<td>Regulament editie: (link)</td>
						<td><input type="text" name="regulament_editie"></td>
					</tr>
					<tr>
						<td>Regulament COSPLAY: (link)</td>
						<td><input type="text" name="regulament_cosplay"></td>
					</tr>
				</table>
				<table id="jocuri">
					<h2>Jocuri:</h2>
					<tr class="head"><td>Nume Joc</td><td>Titlu</td><td>Mod : echipa / single</td><td>Nr. Maxim Jucatori / Echipe</td><td>Regulament Joc</td><td><a href="#" id="adauga_joc" class="button">Adauga joc</a></td></tr>
					<tr>
						<td><input type="text" name="jocuri[]"></td>
						<td><input type="text" name="g_title[]"></td>
						<td><select name="type_jocuri[]" class="game_type"><option value="0">Single</option><option value="1">Team</option></select></td>
						<td><input type="text" name="max_players[]"></td>
						<td><input type="text" name="rules_n_terms[]"></td>
						<td><a href="#" id="sterge_joc" class="button">Sterge joc</a></td>
					</tr>
				</table>
				<table>
					<tr><td><input type="submit" name="adauga_editie_noua" value="Adauga Editie Noua" class="button lp_green"></td></tr>
				</table>
			</form>
			<?php } ?>

</div>