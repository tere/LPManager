<?php
	lp_header();
    $lp_opt =   get_option('lp_options');


    $nr_laptop = lp_count('laptop');
    $nr_pc     = lp_count('pc');
?>

<section class="wrapper">
	<?php if($lp_opt['lp_is_open']) {?>
		<div id="lp_statistici">
		<h2>Statistici</h2>
			<table>
				<tr class="head">
					<td>Joc</td>
					<td>Single/Team</td>
					<td>Numar maxim Jucatori</td>
					<td>Numar Jucatori/echipe Inscrisi/e</td>
					<td>Status</td>
				</tr>
				<?php
					$games = $lp_opt['games'];
					foreach($games as $game) {
						echo '<tr>';
							echo '<td>'. $game['name'] .'</td>';
							echo '<td>'. $game['is_team'] .'</td>';
							echo '<td>'. $game['max_players'] .'</td>';
							echo '<td>'. signs_up($game['is_team'],$game['name']).'</td>';
							echo '<td>'. $game['status'] .'</td>';
						echo '</tr>';
					}
				?>
			</table>
			<table>
				<tr class="head">
					<td>Laptopuri</td>
					<td>Calculatoare</td>
				</tr>

				<tr>
					<td><?php echo $nr_laptop ?></td>
					<td><?php echo $nr_pc; ?></td>
				</tr>
			</table>
		</div>
	<?php } else {?>
		<p><strong>Momentan nu este deschisa nicio editie de Lan Party</strong></p>
	<?php } ?>
</section>
