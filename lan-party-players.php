<?php
	lp_header();
    $lp_opt =   get_option('lp_options');
    if($lp_opt['lp_is_open']) {
    $games= $lp_opt['games'];
    $select_games = '<select name="game"><option value="1">Joc</option>';
    foreach($games as $game) {
    	$select_games .='<option value="'.$game['name'].'">'.$game['name'].'</option>';
    }
    $select_games.='</select>';
}
    global $wpdb;
     if($lp_opt['lp_is_open']) {
    $teams = $wpdb->get_results("SELECT DISTINCT `echipa` FROM {$lp_opt['players_table']} WHERE `cu_echipa`=1",ARRAY_A);
    $select_teams = '<select name="team"><option value="1">Echipa</option>';
    foreach($teams as $team) {
    	$select_teams.='<option value="'.$team['echipa'].'">'.$team['echipa'].'</option>';
    }
    $select_teams.='</select>';
}
    if($lp_opt['lp_is_open']) {
    	 $total_inscrisi = $wpdb->get_var("SELECT COUNT(`nume`) FROM {$lp_opt['players_table']}");
	     $total_echipe   = $wpdb->get_var("SELECT COUNT(DISTINCT `echipa`) FROM {$lp_opt['players_table']} WHERE `cu_echipa`=1");
	     $total_single   = $wpdb->get_var("SELECT COUNT(`nume`) FROM {$lp_opt['players_table']} WHERE `cu_echipa`=0");
    } else {
    	$total_inscrisi = 0;
    	$total_single = 0;
    	$total_echipe = 0;
    }

?>

<section class="wrapper">
	<?php if($lp_opt['lp_is_open']) {?>
		<div id="jucatori">
			<h3>Jucatori</h3>

			<table>
				<tr><td>Total inscrisi : <?php echo $total_inscrisi; ?></td><td>Team : <?php echo $total_echipe; ?></td><td>Single : <?php echo $total_single; ?></td></tr>
			</table>

			<form action="" method="POST">
			<table>
				<tr>
					<td><?php echo $select_games; ?></td>
					<td><?php echo $select_teams; ?></td>
					<td><input type="submit" name="filter" class="button" value="Filtreaza"></td>
				</tr>
			</table>
			</form>
			<table class="lp_table">
				<tr class="head">
					<td>Nr. Crt</td>
					<td>Nume</td>
					<td>Prenume</td>
					<td>CNP</td>
					<td>Email</td>
					<td>Telefon</td>
					<td>Domiciliu</td>
					<td>Joc</td>
					<td>Nickname</td>
					<td>Echipa</td>
					<td>Ce aduce</td>
					<td>Data Inscriere</td>
				</tr>
			<?php if($lp_opt['lp_is_open']) {
				global $wpdb;
				$table = esc_sql($lp_opt['players_table']);



				$pg = isset($_GET['pg']) ? $_GET['pg'] : 1;
				$per_pg = 50;

				$startpoint = ($pg*$per_pg) - $per_pg;

				$query = "SELECT * FROM `{$table}`";

				if(isset($_POST['filter'])) {
					if($_POST['team'] != 1) {
						$query.=' WHERE `echipa`=\''.$_POST['team'].'\'';
					} else {
						$query.=' WHERE 1';
					}
					if($_POST['game'] != 1) {
						$query.=' AND `joc`=\''.$_POST['game'].'\'';
					} else {
						$query.=' AND 1';
					}
				}


				$query .=" ORDER BY `id` ASC LIMIT {$startpoint},{$per_pg}";

				$players = $wpdb->get_results($query, ARRAY_A);

				foreach($players as $pl) {
					if($pl['capitan']){
						$style= "style=\"background:#aebefd\"";
					} else {
						$style ="";
					}
					echo '<tr '.$style.' class="lp_player_delete" data-player="'.$pl['id'].'" >';
						echo '<td>'.$pl['id'].'</td>';
						echo '<td>'.$pl['nume'].'</td>';
						echo '<td>'.$pl['prenume'].'</td>';
						echo '<td>'.$pl['cnp'].'</td>';
						echo '<td>'.$pl['email'].'</td>';
						echo '<td>'.$pl['telefon'].'</td>';
						echo '<td>'.$pl['domiciliu'].'</td>';
						echo '<td>'.$pl['joc'].'</td>';
						echo '<td>'.$pl['nickname'].'</td>';
						echo '<td>'.$pl['echipa'].'</td>';
						echo '<td>'.$pl['ce_aduce'].'</td>';
						echo '<td>'.$pl['date'].'</td>';
					echo '</tr>';
				}


			}?>
			</table>
			<?php echo lp_players_pagination($per_pg, $pg, $url='?'); ?>
		</div>
	<?php } else {?>
	<p><strong>Momentan nu este deschisa nicio editie de Lan Party</strong></p>
	<?php } ?>
</section>

<script>
	jQuery(document).ready(function() {
		jQuery('.lp_player_deletdae').click(function() { //sterge "da" ca sa mearga
			var player_id = jQuery(this).data("player");
			if(window.confirm("Sterge jucator?")) {
				jQuery.ajax({
					type : "POST",
					url: "/wp-admin/admin-ajax.php",
					data: {
						"id": player_id,
						"action": "lp_delete_player"
					}
				}).done(function(data) {
					console.log(data);
					jQuery('tr[data-player="'+player_id+'"]').remove();
				}).fail(function(data) {
					console.log(data);
				});
			return false;
			}


		});
	});
</script>