<?php
	lp_header();
    $lp_opt =   get_option('lp_options');

?>
<script>
	jQuery(document).ready(function() {
		jQuery('#is_team').on('change', function() {
			if (jQuery(this).val() == 1) {
				jQuery('#players_in_team').css('display','table-row');
			}
		});

		jQuery('#genereaza_shortcode').on('click',function() {
			var shortcode = '[lp_form ';
			var game = jQuery('#game').val();
			var is_team =  jQuery('#is_team').val();
			var players = jQuery('#players').val();
			if(is_team == 1) {

				shortcode += 'game="' + game + '" is_team="' + is_team + '" players_in_team="' + players + '"]';
			} else {
				shortcode += 'game="'+game+'" is_team="'+is_team+'"]';
			}

			jQuery("#formulare").append('<p>Shorcodeul generat: ' + shortcode + '</p>');
			jQuery("#formulare").append('<p>Cu copy/paste se introduce in pagina/articolul create pentru acest formular</p>');

		})
	})
</script>

<section class="wrapper">
	<?php if($lp_opt['lp_is_open']) {?>
	<div id="formulare">
		<h3>Formulare</h3>
		<form action="">
			<table>
				<tr>
					<td>Joc:</td>
					<td><select id="game">
						<?php foreach($lp_opt['games'] as $game) {
							echo '<option value="'.$game['name'].'">'.$game['name'].'</option>';
						}?>
					</select></td>
				</tr>
				<tr>
					<td>Pe echipe:</td>
					<td><select  id="is_team">
						<option value=""></option>
						<option value="0">Nu</option>
						<option value="1">Da</option>
					</select></td>
				</tr>
				<tr style="display:none" id="players_in_team">
					<td>Nr. Jucatori in Echipa</td>
					<td><input type="text" name="players_in_team" id="players"></td>
				</tr>
				<tr><td colspan="2"><a href="#" class="button" id="genereaza_shortcode">Genereaza shortcode</a></td></tr>

			</table>

		</form>
	</div>





	<?php } else {?>
	<p><strong>Momentan nu este deschisa nicio editie de Lan Party</strong></p>
	<?php } ?>
</section>